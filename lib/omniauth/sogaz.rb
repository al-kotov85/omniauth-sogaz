require 'dry/monads/result'
require 'dry/monads/do'
require 'dry-initializer'
require 'omniauth/api_user_hash'
require 'omniauth/auth_access_token'
require 'faraday_middleware'
require 'json'

module Omniauth
  class Sogaz
    include Dry::Monads::Result::Mixin
    include Dry::Monads::Do.for(:call)
    extend Dry::Initializer

    param :app_url, default: -> { 'https://lk.sogaz.ru' }

    option :app_id
    option :app_secret

    def call(input)
      code = input[:code].to_s
      return Failure(:empty_code) if code.empty?

      access_token = yield get_auth_access_token.call(code)
      user_hash = yield get_user_hash.call(access_token)

      Success(user_hash)
    end

    private

    def connection
      @connection ||= Faraday.new(connection_options) do |conn|
        conn.use FaradayMiddleware::FollowRedirects
        conn.adapter Faraday.default_adapter
      end
    end

    def get_auth_access_token
      @get_auth_access_token ||= AuthAccessToken.new(connection,
        app_id: app_id,
        app_secret: app_secret
      )
    end

    def get_user_hash
      @get_user_hash ||= ApiUserHash.new(connection)
    end

    def connection_options
      {
        url: app_url,
        headers: { content_type: 'application/json' }
      }
    end
  end
end
