require 'dry/monads/result'
require 'dry-initializer'

module Omniauth
  class AuthAccessToken
    include Dry::Monads::Result::Mixin
    extend Dry::Initializer

    param :connection

    option :app_id
    option :app_secret
    option :endpoint, default: -> { '/auth/oauth2/?response_type=token' }

    def call(code)
      response = connection.post(endpoint, request_body(code))
      status, body = process_response(response)

      status == :ok ? Success(body[:access_token]) : Failure(body[:error])
    end

    def request_body(code)
      body = default_params.merge(code: code)
      body.to_json
    end

    def process_response(response)
      body = JSON.parse(response.body, symbolize_names: true)
      status = response.status == 200 ? :ok : :error

      [status, body]
    end

    private

    def default_params
      Hash(
        client_id: app_id,
        client_secret: app_secret,
        grant_type: 'authorization_code'
      )
    end
  end
end
