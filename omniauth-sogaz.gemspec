Gem::Specification.new do |s|
  s.name         = 'omniauth-sogaz'
  s.version      = '1.0.0'
  s.date         = '2017-07-23'
  s.summary      = 'OmniAuth gem for SogazLK'
  s.description  = 'OmniAuth gem for SogazLK'
  s.authors      = ['Elena Mishina']
  s.email        = 'elena.smolnikova@overteam.ru'
  s.files        = Dir['{lib}/**/*.rb', 'bin/*', 'LICENSE', '*.md']
  s.test_files   = Dir['{spec}/**/*.rb']
  s.require_path = 'lib'
  s.license      = 'MIT'

  s.add_development_dependency "bundler", "~> 1.16"

  s.add_dependency 'faraday_middleware'
  s.add_dependency 'rack'
  s.add_dependency 'dry-initializer'
  s.add_dependency 'dry-monads'
  s.add_development_dependency 'rspec'
end
