require 'omniauth-sogaz'

describe Omniauth::Sogaz do
  describe '#class' do
    context 'with valid params' do
      let(:auth_access_token_instance) { instance_double(Omniauth::AuthAccessToken) }
      let(:api_user_instance) { instance_double(Omniauth::ApiUserHash) }
      let(:access_token) { Dry::Monads.Success('valid_access_token') }
      let(:user_hash) { Dry::Monads.Success(user_hash: :user_hash) }

      before do
        allow(Omniauth::AuthAccessToken).to receive(:new).and_return(auth_access_token_instance)
        allow(auth_access_token_instance).to receive(:call).and_return(access_token)
        allow(Omniauth::ApiUserHash).to receive(:new).and_return(api_user_instance)
        allow(api_user_instance).to receive(:call).and_return(user_hash)
      end

      it 'returns user_hash' do
        expect(described_class.new(app_id: 'app_id', app_secret: 'app_secret').call(code: 'some code')).to eq(user_hash)
      end
    end

    context 'with invalid params' do
      it 'returns :empty_code' do
        expect(described_class.new(app_id: 'app_id', app_secret: 'app_secret').call(code: '')).to eq(Dry::Monads.Failure(:empty_code))
      end

      it 'returns :empty_code' do
        expect(described_class.new(app_id: 'app_id', app_secret: 'app_secret').call({})).to eq(Dry::Monads.Failure(:empty_code))
      end
    end
  end
end
