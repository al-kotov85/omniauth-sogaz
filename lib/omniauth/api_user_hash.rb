require 'dry/monads/result'
require 'dry-initializer'

module Omniauth
  class ApiUserHash
    include Dry::Monads::Result::Mixin
    extend Dry::Initializer

    param :connection

    option :endpoint, default: -> { '/api/oauth2' }

    def call(access_token)
      response = connection.get(endpoint, access_token: access_token)
      status, body = process_response(response)

      status == :ok ? Success(body) : Failure(body[:error])
    end

    def process_response(response)
      body = JSON.parse(response.body, symbolize_names: true)
      status = response.status == 200 ? :ok : :error

      [status, body]
    end
  end
end
